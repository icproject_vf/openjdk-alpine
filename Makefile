VERSION:=$(shell cat version)
NAME=inspiredag/openjdk
DEVELOPEMENT_CA_GIT=https://bitbucket.org/icproject_vf/development-certificate-authority

.PHONY: clean ca build build_only push

clean:
	rm -rf ./certificates

ca:
	git clone $(DEVELOPEMENT_CA_GIT) certificates ; if [ $$? -eq 128 ] ; then echo "There seems to be a CA in place, run make clean to fetch a new version from the repository." ; fi

image: clean ca image_quick

image_quick:
	docker build . --no-cache -t $(NAME):$(VERSION)

push: build
	docker push $(NAME):$(VERSION)
